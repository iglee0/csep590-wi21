grammar Regex;

re
    : (expression|alt|parens) EOF
    ;

expression
    : (elems+=element)*
    ;

alt 
    : (expression) ('|' (expression))*
    ;

parens 
    : l='(' (e=expression|alt|parens) r=')'
    ;


element
    : (a=atom|parens)            # AtomicElement
    | (a=atom|parens) m=MODIFIER # ModifiedElement
    ;

atom
    : c=CHARACTER
    ;

CHARACTER : [a-zA-Z0-9.] ;
MODIFIER : [?*] ;
WS : [ \t\r\n]+ -> skip ;
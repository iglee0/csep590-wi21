import {expect} from "chai";
import {EngExp, EngExpError} from "../src/engexp";
//import {assert} from "chai";

describe("custom", () => {

    // Tests are created with the it() function, which takes a description
    // and the test body, wrapped up in a function.
    it("testing EngExpError invalid flag", () => {
        // how to test for expected error thrown?
        expect(function() {new EngExp().match("a").withFlags("s")}).to.throw(EngExpError);

    });

    // You should modify the above test so it's interesting, and provide
    // at least 4 more nontrivial tests.

    it("disjunctive date pattern with nested levels", () => {
        const month = new EngExp()
            .match("Jan").or("Feb").or("Mar").or("Apr")
            .or("May").or("Jun").or("Jul").or("Aug")
            .or("Sep").or("Oct").or("Nov").or("Dec");
        const e = new EngExp()
            .startOfLine().beginLevel()
            .digit().repeated(1, 2)
            .then("/").beginLevel()
                .digit().repeated(1, 2).endLevel()
            .then("/").beginLevel()
                .digit().repeated(2, 4).endLevel()
            .or(
             new EngExp()
                .digit().repeated(1, 2)
                .then(" ")
                .then(month)
                .then(" ")
                .then(new EngExp().digit().repeated(2, 4))
            )
            .endLevel().endOfLine()
            .asRegExp();
        expect(e.test("12/25/2015")).to.be.true;
        expect(e.test("25 Dec 2015")).to.be.true;
        expect(e.test("11/11/11/11")).to.be.false;
        expect(e.test("111 Dec 9999")).to.be.false;
    });

    it("nested capture group error testing", () => {

        expect(function() {new EngExp().beginCapture().then(
            new EngExp().endCapture()
        )}).to.throw(EngExpError)
    });

    it("level with incomplete capture group should error out", () => {
        expect(function() {
            new EngExp().beginLevel().beginCapture().endLevel().endCapture().asRegExp()
        }).to.throw(EngExpError)
    });

    it("incomplete level should error out", () => {

        expect(function() {
            new EngExp().beginLevel().beginLevel().beginCapture().endCapture().endLevel().endCapture().asRegExp()
        }).to.throw(EngExpError)
    }
    );

    it("should capture nested groups in nested levels", () => {
        const e = new EngExp()
            .startOfLine()
            .then("http")
            .maybe("s")
            .then("://")
            .maybe("www.")
            .beginCapture()
            .beginLevel()
              .beginCapture()
                .anythingBut("/")
              .endCapture()
              .anythingBut(" ")
            .endLevel()
            .endCapture()
            .endOfLine()
            .asRegExp();
        const result = e.exec("https://www.google.com/maps");
        expect(result[1]).to.be.equal("google.com/maps");
        expect(result[2]).to.be.equal("google.com");
    });

});

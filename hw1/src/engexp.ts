export class EngExpError extends Error {}

export class EngExp {
    private flags: string = "m";
    private pattern: string = "";
    // You can add new fields here
    private prev_level: EngExp;
    public trail: string = "";

    // Don't change sanitize()
    private static sanitize(s: string | EngExp): string | EngExp {
        if (s instanceof EngExp)
            return s;
        else {
            return s.replace(/[^A-Za-z0-9_]/g, "\\$&");
        }
    }

    // Don't change the following three public methods either
    public valueOf(): string {
        return this.asRegExp().source;
    }

    public toString(): string {
        return this.asRegExp().source;
    }

    public withFlags(flags: string) {
        if (/[^gimuy]/g.test(flags))
            throw new EngExpError("invalid flags");
        this.flags = "".concat(...new Set(flags));
        return this;
    }
    // End methods you shouldn't touch

    // asRegExp() will always be called before using your EngExp as a RegExp,
    // or before converting it into a string (for example to include via ``
    // template literals in another EngExp). You might want to take advantage
    // of this for debugging or error checking.

    public matchTrail = function (trail) {
        let parens = { '(':')', '[':']'}

        // trail has been keeping track of levels and groups as parentheses. this function
        // catches both incomplete level or incomplete capture groups
        let stack = [];
        
        for (let i = 0; i < trail.length; i++){
            if (trail[i] in parens) {
                stack.push(trail[i])
            }
            else {
                // get the element immediately before
                let temp = stack.pop();
                // if the parens don't close, return false
                if (parens[temp] != trail[i]) {
                    return false
                };
            }
        }
        if (stack.length != 0) {
            return false
        };
        return true;
    }

    public asRegExp(): RegExp {
        if (this.matchTrail(this.trail) === false) {
            throw new EngExpError("there's either incomplete level or incomplete capture group")
        }

        return new RegExp(this.pattern, this.flags);
    }

    // There is a bug somewhere in this code... you'll need to find it

    public match(literal: string): EngExp {
        return this.then(literal);
    }

    public then(pattern: string | EngExp): EngExp {
        this.pattern += `(?:${EngExp.sanitize(pattern)})`;
        return this;
    }

    public startOfLine(): EngExp {
        this.pattern += "^";
        return this;
    }

    public endOfLine(): EngExp {
        this.pattern += "$";
        return this;
    }

    public zeroOrMore(pattern?: EngExp): EngExp {
        if (pattern)
            return this.then(pattern.zeroOrMore());
        else {
            this.pattern = `(?:${this.pattern})*`;
            return this;
        }
    }

    public oneOrMore(pattern?: EngExp): EngExp {
        if (pattern)
            return this.then(pattern.oneOrMore());
        else {
            this.pattern = `(?:${this.pattern})+`;
            return this;
        }
    }

    public optional(): EngExp {
        this.pattern = `(?:${this.pattern})?`;
        return this;
    }

    public maybe(pattern: string | EngExp): EngExp {
        this.pattern += `(?:${EngExp.sanitize(pattern)})?`;
        return this;
    }

    public anythingBut(characters: string): EngExp {
        this.pattern += `[^${EngExp.sanitize(characters)}]*`;
        return this;
    }

    public digit(): EngExp {
        this.pattern += "\\d";
        return this;
    }

    public repeated(min: number, max?: number): EngExp {
        if (max === undefined) {
            this.pattern = `(?:${this.pattern}){${min}}`;
        }
        else {
            this.pattern = `(?:${this.pattern}){${min},${max}}`;
        }
        return this;
    }

    public multiple(pattern: string | EngExp, min: number, max?: number) {
        if (max === undefined) {
            this.pattern += `(?:${EngExp.sanitize(pattern)}){${min}}`;
        }
        else {
            this.pattern += `(?:${EngExp.sanitize(pattern)}){${min},${max}}`;
        }
        return this;
    }

    // You need to implement these five operators:

    public or(pattern: string | EngExp): EngExp {
        if (typeof pattern === 'string') {
            this.pattern = this.pattern.slice(0,-1) + `|${EngExp.sanitize(pattern)})`
        }
        else {
            this.pattern = `(?:${this.pattern}|${EngExp.sanitize(pattern)})`
        }
        return this
    }

    public beginCapture(): EngExp {
        this.pattern += `(`
        this.trail += `(`
        return this
    }

    public endCapture(): EngExp {
        this.pattern += `)`
        this.trail += `)`
        return this
    }

    public beginLevel(): EngExp {
        const level = new EngExp()
        level.prev_level = this
        level.trail += `[`
        return level
    }

    public endLevel(): EngExp {
        try {
            new RegExp(this.pattern)
        }
        catch(err){
            throw new EngExpError("this level contains invalid regex")    
        }
        this.trail += `]`
        this.prev_level.trail += this.trail
        return this.prev_level.then(this)
    }
}

// ----- EXTRA CREDIT: named capture groups -----

// You'll need to modify a few parts of your EngExp implementation.
// First, change the signatures of beginCapture() and endCapture() to the following:
//
//     beginCapture(name?: string): EngExp
//     endCapture(name?: string): EngExp
//
// JavaScript regular expressions don't support named groups out of the box,
// so we'll need to extend them. Change the signature of asRegExp() to the following:
//
//     asRegExp(): NamedRegExp
//
// The interface for NamedRegExp is provided below (don't change it!) and depends on
// the new interface for NamedRegExpExecArray.

interface NamedRegExpExecArray extends RegExpExecArray {
    groups: Map<string, string | undefined>;
}

interface NamedRegExp extends RegExp {
    exec(s: string): NamedRegExpExecArray | null;
}

// Essentailly, a NamedRegExp is like a RegExp, but when you call its exec() method
// you get back an array with an extra field, groups, which is a hashmap from the
// names of capture groups to what those groups matched. You can access it like this:
//
//     let r = new EngExp().beginCapture("justf").then("f").endCapture().then("oo").asRegExp()
//     r.exec("foo").groups["justf"]  ==  "f"
//
// See the tests at the end of test/engexp.ts for more usage examples.

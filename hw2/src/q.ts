/* Q is a small query language for JavaScript.
 *
 * Q supports simple queries over a JSON-style data structure,
 * kinda like the functional version of LINQ in C#.
 *
 */

// This class represents all AST Nodes.
export class ASTNode {
    public readonly type: string;

    constructor(type: string) {
        this.type = type;
    }

    execute(data: any[]): any {
        return Array.from(data)
    }

    optimize(): ASTNode {
        return this;
    }

    run(data: any[]): any {
        return this.optimize().execute(data);
    }

    //// 1.5 implement call-chaining

    filter(predicate: (datum: any) => boolean): ASTNode {
        //throw new Error("Call chaining not implemented.");
        return new ThenNode(this, new FilterNode(predicate))
    }

    apply(callback: (datum: any) => any): ASTNode {
        //throw new Error("Call chaining not implemented.");
        return new ThenNode(this, new ApplyNode(callback))
    }

    count(): ASTNode {
        //throw new Error("Call chaining not implemented.");
        return new ThenNode(this, new CountNode())
    }

    product(query: ASTNode): ASTNode {
        //throw new Error("Call chaining not implemented.");
        return new ThenNode(this, new CartesianProductNode(this, query))
    }

    join(query: ASTNode, relation: string | ((left: any, right: any) => any)): ASTNode {
        //throw new Error("Call chaining not implemented.");
        //return new ThenNode(this, new JoinNode(this, query, relation))
        let predicate;

        if (typeof(relation) === "string") {
            predicate = datum => relation in datum.left
                && relation in datum.right
                && datum.left[relation] === datum.right[relation] as FieldPredicate;
            predicate.field = relation;
        } else {
            predicate = datum => relation(datum.left, datum.right);
        }
        return new ThenNode(new ThenNode(
            this.product(query),
            new FilterNode(predicate)),
            new ApplyNode(datum => Object.assign({}, datum.left, datum.right)));
    }
}

// The Id node just outputs all records from input.
export class IdNode extends ASTNode {

    constructor() {
        super("Id");
    }

    //// 1.1 implement execute
    execute(data: any[]): any {
        return Array.from(data)
    }
}

// We can use an Id node as a convenient starting point for
// the call-chaining interface.
export const Q = new IdNode();

// The Filter node uses a callback to throw out some records.
export class FilterNode extends ASTNode {
    predicate: (datum: any) => boolean;

    constructor(predicate: (datum: any) => boolean) {
        super("Filter");
        this.predicate = predicate;
    }

    //// 1.1 implement execute
    execute(data: any[]): any {
        return data.filter(this.predicate)
    }
}

// The Then node chains multiple actions on one data structure.
export class ThenNode extends ASTNode {
    first: ASTNode;
    second: ASTNode;

    constructor(first: ASTNode, second: ASTNode) {
        super("Then");
        this.first = first;
        this.second = second;
    }

    //// 1.1 implement execute
    execute(data: any[]): any {
        let result = this.first.execute(data)
        return this.second.execute(result)
    }

    optimize(): ASTNode {
        return new ThenNode(this.first.optimize(), this.second.optimize());
    }
}

//// 1.3 implement Apply and Count Nodes

export class ApplyNode extends ASTNode {
    callback: any;
    constructor(callback: (datum: any) => any) {
        super("Apply");
        this.callback = callback
    }

    execute(data: any[]): any {
        return data.map(this.callback)
    };
}

export class CountNode extends ASTNode {
    constructor() {
        super("Count");
        //throw new Error("Unimplemented AST node " + this.type);
    }
    execute(data:any[]): any{
        return [data.length]
    };
}

//// 2.1 optimize queries

// This function permanently adds a new optimization function to a node type.
// An optimization function takes in a node and either returns a new,
// optimized node or null if no optimizations can be performed. AddOptimization
// will register this function to a particular node type, so that it will be called
// as part of that node's optimize() method, along with all other registered
// optimizations.
function AddOptimization(nodeType, opt: (this: ASTNode) => ASTNode | null) {
    const oldOptimize = nodeType.prototype.optimize;
    nodeType.prototype.optimize = function(this: ASTNode): ASTNode {
        const newThis = oldOptimize.call(this);
        return opt.call(newThis) || newThis;
    };
}

// For example, the following small optimization removes unnecessary Id nodes.
AddOptimization(ThenNode, function(this: ThenNode): ASTNode {
    if (this.first instanceof IdNode) {
        return this.second;
    } else if (this.second instanceof IdNode) {
        return this.first;
    } else {
        return null;
    }
});

// The above optimization has a few notable side effects. First, it removes the
// root IdNode from your call chain, so if you were depending on that to exist
// your other optimizations may be confused. Also, it returns the interesting
// subtree directly, without trying to optimize it more. It is up to you to
// determine where additional optimization should occur.

// We won't specifically test for this optimization, so if it's giving you
// a headache feel free to comment it out.

// You can add optimizations for part 2.1 here (or anywhere below).
// ...
AddOptimization(ThenNode, function(this: ThenNode): ASTNode {
    if (this.first instanceof FilterNode && this.second instanceof FilterNode) {
            let f = (<FilterNode>this.first).predicate;
            let g = (<FilterNode>this.second).predicate;
            return new FilterNode(datum => f(datum) && g(datum));
    }
        if (this.first instanceof ThenNode
            && (<ThenNode>this.first).second instanceof FilterNode
            && this.second instanceof FilterNode) {
            let then = <ThenNode>this.first;
            let f = (<FilterNode>then.second).predicate;
            let g = (<FilterNode>this.second).predicate;
            return new ThenNode(then.first, new FilterNode(datum => f(datum) && g(datum)));
    }
        return null;

});

//// 2.2 internal node types and CountIf

export class CountIfNode extends ASTNode {
    predicate: (datum: any) => boolean;

    constructor(predicate: (datum: any) => boolean) {
        super("CountIf");
        this.predicate = predicate;
    }

    execute (data: any[]): any {
        //console.log(data)
        //console.log(this.predicate(data))
        return [data.filter(this.predicate).length]
    }

}

AddOptimization(ThenNode, function (this: ThenNode): ASTNode {
    if (this.first instanceof FilterNode
        && this.second instanceof CountNode) {
        return new CountIfNode((<FilterNode>this.first).predicate);
    }
    if (this.first instanceof ThenNode
        && (<ThenNode>this.first).second instanceof FilterNode
        && this.second instanceof CountNode) {
        let then = <ThenNode>this.first;
        let filter = <FilterNode>then.second;
        return new ThenNode(then.first, new CountIfNode(filter.predicate));
    }
    return null;
});

//// 3.1 cartesian products

export class CartesianProductNode extends ASTNode {
    left: ASTNode;
    right: ASTNode;

    constructor(left: ASTNode, right: ASTNode) {
        super("CartesianProduct");
        this.left = left;
        this.right = right;
        //throw new Error("Unimplemented AST node " + this.type);
    }

    execute(data: any[]): any {
        let left_result = this.left.execute(data);
        let right_result = this.right.execute(data);
        var prod = []

        for (var i=0; i<left_result.length; i++) {
            for (var j=0; j<right_result.length; j++) {
                prod.push({"left" : left_result[i], "right":right_result[j]});
            }
        }
        return prod
    };
}

//// 3.2-3.6 joins and hash joins

/* Hint: Recall from the spec that a join of two arrays P and Q by a
   function f(l, r) is the array with one entry for each pair of a record
   in P and Q for which f returns true and that each entry should have all
   fields and values that either record in the pair had.

   Most notably, if both records had the same field, use the value from the
   record from Q. In JavaScript, this can be achieved with Object.assign:
   https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign
*/

export class JoinNode extends ASTNode {
    left: ASTNode;
    right: ASTNode;
    prod: ASTNode;
    relation: (left: any, right: any) => any | FieldPredicate;

    constructor(left: ASTNode, right: ASTNode, relation) {
        super("Join");
        this.left = left
        this.right = right
        this.prod = new CartesianProductNode(this.left, this.right);
        this.relation = relation;
    }

    execute(data: any[]): any {
        let results = this.prod.execute(data);
        let joins = results.filter(this.relation);
        return joins.map(x => Object.assign({}, x.left, x.right));
    };
}

interface FieldPredicate {
    (datum: any): boolean;
    field: string;
}

AddOptimization(ThenNode, function (this: ThenNode): ASTNode {
    if (this.first instanceof ThenNode
        && (<ThenNode>this.first).first instanceof CartesianProductNode
        && (<ThenNode>this.first).second instanceof FilterNode
        && !("field" in (<FilterNode>(<ThenNode>this.first).second).predicate)
        && this.second instanceof ApplyNode) {
        let then = <ThenNode>this.first;
        let product = <CartesianProductNode>then.first;
        let filter = <FilterNode>then.second;
        return new JoinNode(product.left, product.right, filter.predicate);
    }
    return null;
});

/* Hint: While optimizing the fluent join to use your internal JoinNode,
   if you find yourself needing to compare two functions for equality,
   you can do so with the Javascript operator `===` (pointer equality).
 */

export class HashJoinNode extends ASTNode {
    key: string;
    left: ASTNode;
    right: ASTNode;


    constructor(key, left, right) { // you may want to add some proper arguments to this constructor
        super("HashJoin");
        //throw new Error("Unimplemented AST node " + this.type);
        this.left = left;
        this.right = right;
        this.key = key;
    }

    execute(data: any[]): any {
        let left_table = this.makeHashTable(this.left.execute(data))
        let right_table = this.makeHashTable(this.right.execute(data))
        let joins = []
        
        for (var key of Object.keys(left_table)) {
            let left = left_table[key]
            let right = right_table[key] || {}

            for (var i = 0; i < left.length; i++){
                for (var j=0; j < right.length; j++){
                    joins.push({"left" : left[i], "right" : right[j]})
                }
            }

        }
        
        return joins;
    }

    makeHashTable(data){
        let unique_keys = new Set(data.map(x => x[this.key]))
        let d = {}
        Array.from(unique_keys).forEach((x: string) => d[x] = [])
        for (var i=0; i<data.length; i++){
            d[data[i][this.key]].push(data[i])
        }

        return d;
    }
}

AddOptimization(ThenNode, function (this: ThenNode): ASTNode {
    if (this.first instanceof ThenNode
        && (<ThenNode>this.first).first instanceof CartesianProductNode
        && (<ThenNode>this.first).second instanceof FilterNode
        && "field" in (<FilterNode>(<ThenNode>this.first).second).predicate
        && this.second instanceof ApplyNode) {
        let then = <ThenNode>this.first;
        let product = <CartesianProductNode>then.first;
        let predicate = <FieldPredicate>(<FilterNode>then.second).predicate;
        return new HashJoinNode(predicate.field, product.left, product.right);
    }
    return null;
});

import * as q from "./q";
const Q = q.Q; // IdNode
//import {crimeData} from "../test/data";

//// 1.2 write a query

export const theftsQuery = new q.ThenNode(new q.IdNode(), new q.FilterNode(crime => crime[2].match(/THEFT/)));
export const autoTheftsQuery = new q.ThenNode(new q.IdNode(), new q.FilterNode(crime => crime[3].match(/MOTOR/)));

//// 1.4 clean the data

export const cleanupQuery = new q.ApplyNode(crime => ({"description":crime[2],"category":crime[3],"area":crime[4],"date":crime[5]}));

//// 1.6 reimplement queries with call-chaining

export const cleanupQuery2 = Q.apply(crime => ({"description":crime[2],"category":crime[3],"area":crime[4],"date":crime[5]}));
export const theftsQuery2 = Q.filter(crime => crime.description.match(/THEFT/));
export const autoTheftsQuery2 = Q.filter(crime => crime.category.match(/MOTOR/));

//// 4 put your queries here (remember to export them for use in tests)

import {expect} from "chai";

import * as q from "../src/q";
const Q = q.Q;

// You should provide at least 5 tests, using your own queries from Part 4
// and either the crime data from test/data.ts or your own data. You can look
// at test/q.ts to see how to import and use the queries and data.
// The outline of the first test has been provided as an example - you should
// modify it so it actually does something interesting.

describe("custom", () => {

    it("put description of test 1 here", () => {
        const query = Q;
        expect(query.type).to.be.equal("Id");
    });

});
